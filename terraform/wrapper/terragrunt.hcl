remote_state {
  backend = "s3"
  config = {
    bucket  = "octo-tfstate-bucket-${get_env("TRIGRAM")}"
    key     = "devskool/${path_relative_to_include()}/terraform.tfstate"
    region  = "eu-west-1"
    profile = "skool"
    encrypt = true
  }
}


inputs = {
  region       = "eu-west-1"
  environment  = "devskool"
  profile      = "skool"
  project_name = "octo-cloud"
  trigramme    = get_env("TRIGRAM")
  bucket_name  = "octo-lab-bucket-data-${get_env("TRIGRAM")}"
}
