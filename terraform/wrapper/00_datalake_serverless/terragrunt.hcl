terraform {
  source = "../../modules/datalake_components"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  lambda_zip_dependencies_path = "${get_terragrunt_dir()}/../../libraries_lambda_layer.zip"
  lambda_code_path = "${get_terragrunt_dir()}/../../../lambda_processing/src/lambda_main_app.py"
}