terraform {
  source = "../../modules/sql_engine"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  athena_db_name = "job_hunter"
  athena_results_key_bucket_name = "athena_results"
  processed_job_offers_key_name = "job_offers/processed"
  bucket_name = "octo-data-tp-data-eng-${get_env("TRIGRAM")}"

}