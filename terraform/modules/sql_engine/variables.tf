variable "profile" {
  type = string
}

variable "region" {
  type = string
}

variable "trigramme" {
  type = string
}
# ------------------ Athena -----------------------------

variable "athena_db_name" {
  description = "Nom de la db Athena"
  type        = string
}

variable "athena_results_key_bucket_name" {
  description = "Chemin vers le bucket pour stocker les résultats des requêtes Athena"
  type        = string
}

variable "processed_job_offers_key_name" {
  description = "Chemin menant aux offres d'emploies traitées par la lambda"
  type        = string
}

variable "bucket_name" {
  description = "Bucket target d'athena"
  type        = string
}