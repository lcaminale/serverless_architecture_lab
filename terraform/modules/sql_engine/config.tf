provider "aws" {
  version = "~> 2.41.0"
  region  = var.region
  profile = var.profile
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
  required_version = ">= 0.12"
}
