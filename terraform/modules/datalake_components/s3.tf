# TODO : Pensez à rajouter l'option permettant la destruction du bucket
resource "aws_s3_bucket" "s3_data_bucket" {
  bucket        = "octo-data-tp-data-eng-${var.trigramme}"
  force_destroy = true
  tags = {
    Name = "octo-lab"
  }
}
resource "aws_s3_bucket_object" "folder_raw_data" {
  depends_on = [
  aws_s3_bucket.s3_data_bucket]
  bucket = aws_s3_bucket.s3_data_bucket.id
  key    = "job_offers/raw/"
  source = "/dev/null"
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3_data_bucket.id

  topic {
    topic_arn     = aws_sns_topic.topic_sns.arn
    events        = ["s3:ObjectCreated:*"]
    filter_prefix       = aws_s3_bucket_object.folder_raw_data.key
    filter_suffix       = ".csv"
  }
}