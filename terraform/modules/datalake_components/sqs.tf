# TODO :  Create sqs queue avec votre trigramme

resource "aws_sqs_queue" "sqs_queue" {
  name                       = "sqs-queue-tp-data-eng-${var.trigramme}"
  receive_wait_time_seconds  = 20
  visibility_timeout_seconds = 600
  redrive_policy             = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.dead_letters_queue.arn}\",\"maxReceiveCount\":1}"
}
# TODO :  Create sqs dead letter queue avec votre trigramme

resource "aws_sqs_queue" "dead_letters_queue" {
  name                      = "dead-letters-sqs-queue-tp-data-eng-${var.trigramme}"
  receive_wait_time_seconds = 20
}

resource "aws_sqs_queue_policy" "results_updates_queue_policy" {
    queue_url = aws_sqs_queue.sqs_queue.id

    policy = data.template_file.policy_sqs_json_sns_event.rendered
}

resource "aws_lambda_event_source_mapping" "event_mapping_sqs_production_lambda" {
  batch_size       = 1
  event_source_arn = aws_sqs_queue.sqs_queue.arn
  function_name    = aws_lambda_function.lambda.arn
  enabled          = true
}
