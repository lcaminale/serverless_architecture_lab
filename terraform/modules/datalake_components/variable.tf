# --------------------- common variables ----------------------

variable "profile" {
  type = string
}

variable "region" {
  type = string
}

variable "environment" {
  type = string
}

variable "bucket_name" {
  type = string
}

variable "trigramme" {
  type = string
}


variable "lambda_zip_dependencies_path" {
  type = string
}

variable "lambda_code_path" {
  type = string
}

