resource "aws_sns_topic" "topic_sns" {
  name = "sns-tp-data-eng-topic-${var.trigramme}"

  policy = data.template_file.policy_sns_json_s3_event.rendered
}

resource "aws_sns_topic_subscription" "sns_send_event_to_sqs" {
  topic_arn = aws_sns_topic.topic_sns.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.sqs_queue.arn
}

# TODO : Ajouter le numéro de téléphone donné dans le lab et le variabiliser
resource "aws_sns_topic_subscription" "sns_send_message_to_user" {
  topic_arn = aws_sns_topic.topic_sns.arn
  protocol  = "sms"
  endpoint  = "+3306XXXXXXXX"
}
