resource "aws_iam_role" "iam_for_lambda" {
  name = "${var.trigramme}.role.lambda"

  assume_role_policy = file("${path.module}/iam_role_and_policies/role.tpl")
}

resource "aws_iam_policy" "lambda_policy" {
  name        = "lambda_data_processing_policy"
  policy      = data.template_file.policy_lambda_json.rendered
}

resource "aws_iam_policy_attachment" "attach_lambda_policy" {
  name       = "lambda_attached_policy"
  policy_arn = aws_iam_policy.lambda_policy.arn
  roles      = [aws_iam_role.iam_for_lambda.name]
}

# TODO : Création de la lambda

resource "aws_lambda_function" "lambda" {
  function_name = "lambda_data_processing"
  filename = data.archive_file.zip_code_lambda.output_path
  role          = aws_iam_role.iam_for_lambda.arn
  description   = "Cette lambda transforme les .csv en .parquet et enrichie les timeseries avec des topologies"
  handler       = "lambda_main_app.lambda_handler"
  runtime       = "python3.7"
  timeout       = 300
  layers = [aws_lambda_layer_version.lambda_layer.arn]
}

resource "aws_lambda_layer_version" "lambda_layer" {
  filename   = var.lambda_zip_dependencies_path
  layer_name = "lambda_data_processing_dependencies_layer"

  compatible_runtimes = ["python3.7"]
}
