# Lab datalake serverless

* **Durée : 2H**

* 1H de terraform
* 30 min autour de l'utilisation de la console et la CLI AWS
* 5 min destroy l'infra
* bonus :  
    * 15 min de présentation et utilisation d'athéna

## Prerequis


* Installer ansible AWS cli [ici](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv1.html)

*ou via pip :* 

```bash
pip install awscli
```

* Installer terraform via :
    * En téléchargeant le binaire : [ici](https://www.terraform.io/downloads.html)
    * En installant : [tfenv](https://github.com/tfutils/tfenv) 


* Installer terragrunt via :
    * En téléchargeant le binaire : [ici](https://terragrunt.gruntwork.io/docs/getting-started/install/)
    * En installant : [tgenv](https://github.com/cunymatthieu/tgenv)
     
## Overview


Durant ce Lab, vous allez devoir automatiser la création de l'infrastructure d'un datalake serverless.

Cette infrastructure sera composée:

* d'un Bucket [S3](https://aws.amazon.com/fr/s3/faqs/) pour le stockage
* d'un [sns](https://aws.amazon.com/fr/sns/faqs/), le service de messagerie pub-sub
* d'une queue [sqs](https://aws.amazon.com/fr/sqs/faqs/), la queue managée aws
* d'une [lambda](https://aws.amazon.com/fr/lambda/faqs/) chargée de traiter la donnée entrante
* d'un moteur SQL [athena](https://aws.amazon.com/fr/athena/faqs/) pour s'interfacer avec les données du S3


Ce TP étant axé sur la création d'infrastructure via terraform et les services aws,
nous vous fournirons le code applicatif de la lambda.

*Voici le schéma de l'architecture finale.*
![archi finale](docs/img/finale_archi.png)

## Les étapes du tp:

Avant de commencer, il va falloir exporter une variable d'environnement qui sera votre (Tri/quadri)gramme pour 
differencier vos resources.

Comment faire ?

```bash
export TRIGRAM=lci
``` 

:warning: Votre trigramme doit être en minuscule !

#### 1. Création du bucket pour stocker les données d'entrées (brutes) et les données de sorties (traitées)

**TODO :** Veuillez ajouter un paramètre qui va permettre de forcer la destruction du bucket même s’il est rempli ! 
indice sur la [doc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) 

Le fichier à éditer est ici : [s3.tf](terraform/modules/datalake_components/s3.tf)

Après cette étape vous en êtes ici :

![step_1](docs/img/steps_archi/step_1.png)

*Vous allez avoir une imbrication de dossier dans votre bucket comme ci-dessous:*

![folders_s3](docs/img/folders_s3.png)

#### 2. Création d'un PUB/SUB sns

Documentation sur comment créer un sns via terraform : [ici](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic)

Lorsque vous allez déposer un fichier dans votre bucket s3, celui-ci va publier une notification dans le sns.
Votre sns devra être configuré pour à la fois envoyer un sms + envoyer un message dans une file d'attente.

Les étapes suivantes ont été réalisées :

1. Création d'une file sns
2. Linker le bucket avec sns
3. Envoyer un message

**TODO :** Variabiliser le numéro qu'on vous donnera pendant le lab.

Après cette étape vous en êtes ici :

![step_2](docs/img/steps_archi/step_2.png)


#### 3. Création file d'attente sqs

**TODO :**
Création de 2 queue sqs voir la doc : [ici](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sqs_queue)

La queue sqs va permettre de **découpler** la sns et la lambda. Si la lambda part en erreur, il y a une politique de retry du message
et après un max retry atteint il partira dans une autre queue (Spoiler alert... La dead letter queue !). 

:warning: Donc pensez bien à la `redrive_policy` pour rediriger les "lettres mortes" dans la `dead_queue` !

Après cette étape vous en êtes ici :

![step_3](docs/img/steps_archi/step_3.png)

#### 4. Création d'une lambda 

La lambda va faire le traitement de la donnée, cad :

* Mettre sous bon format la donnée brute
* Ecrire sur le bucket s3 en partitionnant la donnée sous format Parquet

Tout va se passer dans le fichier `lambda.tf`

Il faut créer une lambda avec :
    
    * Le nom:  `lambda_data_processing + votre trigramme
    * Le role : prendre le rôle qui a été définit dans le fichier
    * Le handler : "lambda_main_app.lambda_handler"
    * Le timeout fixé à 1 min
    * Le Layers contenant les dépendances fourni dans le fichier
    * Le runtime "python3.7"
    * Le nom du fichier : en utilisant le datasource --> `data.archive_file.zip_code_lambda.output_path`
 
 Voici la [doc terraform lambda](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function#example-usage) pour vous aider ;-)


Après cette étape vous en êtes ici :

![step_4](docs/img/steps_archi/step_4.png)

#### 6. Lancer la création des vos ressources:

C'est maintenant que les choses sérieuses commencent... Essayez de déployer toutes vos ressources grâce à TerraGrunt (une surcouche à Terraform) !

On doit se placer au niveau de la layer pour pouvoir apply les changements, exemple :

```sh
cd terraform/wrapper/00_datalake_serverless # la layer ici est 00_datalake_serverless
terragrunt apply
```

Et le tour est joué !

Pour les flemmards, on vous a préparé un Makefile !

#### 6. Upload du fichier :

Si vous n'avez pas fait tous les prérequis, installez la cli aws (aide par [ici](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv1.html)).

Maintenant, il ne nous reste plus qu'à tester tout ça... #MomentDeVérité

1. Sur l'UI (console d'aws), essayez d'uploader les csv qui se trouvent dans : `data/*.csv`
2. Automatiser ça en ligne de commande via la cli aws

#### 7. (Bonus) : Athena


Tout le code terraform est donné, vous avez juste à l'appliquer via la commande dans ce répertoire [ici](terraform/wrapper/01_sql_engine):

```bash
terragrunt apply
```

