# ----------------------- PYTHON ------------------------

.PHONY:lint-app
lint:
	cd src && black -l 120 .

# ----------------------- TERRAGRUNT ------------------------

.PHONY:delete-terragrunt-cache
delete-terragrunt-cache:
	rm -rf terraform/wrapper/00_datalake_serverless/.terragrunt-cache
	rm -rf terraform/wrapper/01_sql_engine/.terragrunt-cache

.PHONY:apply-all
apply-all:
	cd terraform/wrapper && terragrunt apply-all


# ----------------------- TERRAGRUNT DATALAKE ------------------------

.PHONY:apply-datalake
apply-datalake:
	cd terraform/wrapper/00_datalake_serverless && terragrunt apply

.PHONY:destroy-datalake
destroy-datalake:
	cd terraform/wrapper/00_datalake_serverless && terragrunt destroy



# ----------------------- TERRAGRUNT SQL ENGINE ------------------------

.PHONY:apply-sql-engine
apply-sql-engine:
	cd terraform/wrapper/01_sql_engine && terragrunt apply

.PHONY:destroy-sql-engine
destroy-sql-engine:
	cd terraform/wrapper/01_sql_engine && terragrunt destroy


